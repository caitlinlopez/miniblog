/* global Backendless */

$(function (){
   var APPLICATION_ID = "5A440563-A0F5-12D8-FFA2-E5CE3907D400",
       SECRET_KEY = "D3E79385-74E2-3838-FF6D-0B7EA9CCFC00",
       VERSION = "v1";
       
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    /*
    var dataStore = Backendless.Persistence.of(Posts);
    var post = new Posts({title: "My First Blog Post", content: "My first blog post content", email:"caitlinlopez28@gmail.com"});
    dataStore.save(post);
    */
   
    var PAGESIZE = 100;
    var dataQueryMain = new Backendless.DataQuery();
    dataQueryMain.options = {
        pageSize:PAGESIZE
    };
    var postsCollection = Backendless.Persistence.of(Posts).find(dataQueryMain);
    
    var date = new Date();
    var day = date.getDate();
    var monthIndex = date.getMonth() + 1; //month index is zero based
    var year = date.getFullYear();
    var dateCondition = monthIndex + "/" + day + "/" + year;
    
    console.log(dateCondition);
    
    var dataQuery = {
    condition: "created >= '" + dateCondition + "'"};
    var postsCollectionToday = Backendless.Persistence.of(Posts).find(dataQuery);
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data, 
        postsCount: postsCollectionToday.totalObjects
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
   
    $('.main-container').html(blogHTML);
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}